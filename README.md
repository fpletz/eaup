# EuroScope European Airspace Use Plan Service

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Description

This project is based on
webscraping [eurocontrol NOP website](https://www.public.nm.eurocontrol.int/PUBPORTAL/gateway/spec/).
We get areas from section `European AUP/UUP` and then `RSA Allocations`.

## Implementation

In practical terms, at `TopSkySettings.txt` config should look like this:
```
HTTP_NOTAM_Area_Sched_Text= 
HTTP_NOTAM_Server={domain}/api/topsky/areas/{your_country_code}/notam/
HTTP_NOTAM_Page_Prefix=?loc=
HTTP_NOTAM_Page_Suffix=
```

## Endpoint Documentation

- You can access the documentation at {domain}/api/docs
  ![img.png](img.png)

## Dependencies

You need to have running [selenium/standalone-chrome](https://hub.docker.com/r/selenium/standalone-chrome)

- `docker run -d --restart allways selenium/standalone-chrome`

## Installation

Even tho this is suposed to be a central service, here is how you deploy it.

Two ways:

- Docker
    - `docker build . -t eaup`
    - `docker run -d --restart allways eaup`
- Python (in your virtual env preferably)
    - `pip install -r ./requirements.txt`
    - `python ./run.py`

## Enviroment Variables

You need to set up:

- `SELENIUM_SERVER`
- `TIME_TO_LIVE_MINUTES`
    - This sets how much time eurocontrol data is considered outdated and requires update

## Features

### Time to Live

This app is configured with time to live data, meaning that beyond a certain time period data is considered outdated and
therefore in need of updating.

### TOPSKY Endpoints

The logic behind was made with Portugal vACC requirements. TODO: Explain

To Be Discussed: This project contain each vacc topsky area parsing logic



## Known Bugs

- To Be Discussed: Two same areas, activated in the same time but with two different altitude ranges, don't squash into
  one.

## Authors and acknowledgment

Authors:

- Rodrigo - 1438868

Acknowledgment:
- Raul - 1058098
- Bernardo - 1096507
- Ricardo - 1110850
- Matisse - 1385143
