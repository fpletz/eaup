import redis

from redis.commands.search.field import TextField


r = redis.Redis(host="cache", port=6379, decode_responses=True)
r.ping()

schema = TextField("$..area", as_name="area")


def get_pools() -> list:
    return list(r.scan_iter())


def get_pool(key: str, filter: str = "") -> list:
    return r.json().get(key, f"${filter}")


def write_pool(key: str, value: dict, directory: str = "$") -> None:
    r.json().set(key, directory, value)


def wipe_pool() -> None:
    r.flushdb()
