import re
from fastapi import APIRouter
from eaup.api.v3.areas import areas, convert_dfs, convert_eaup, convert_sparx
from eaup.api.v3.pools import get_pool, get_pools, wipe_pool, write_pool
from eaup.api.v3.schemas import Area
from eaup.dfs import get_dfs_areas
from eaup.eaup import get_eaup
from eaup.sparx import get_sparx_data


v3_api = APIRouter(prefix="/v3", tags=["v3"])


@v3_api.get("/pools")
def api_get_pools():
    return get_pools()


@v3_api.get("/pool/{key}/")
def api_get_pool(key: str, filter: str = ""):
    return get_pool(key, filter)


def filter_countries_from_string(raw_string: str) -> list[str]:
    return re.findall(r"(\w{2})*", raw_string.upper())


def filter_areas_from_list_countries(
    areas: list[Area], countries: list[str]
) -> list[Area]:
    return [area for area in areas if area.name[:2].upper() in countries]


@v3_api.get("/areas")
def api_get_areas(countries: str = ""):
    if countries:
        return filter_areas_from_list_countries(
            areas=areas(), countries=filter_countries_from_string(countries)
        )
    return areas()


@v3_api.get("/countries")
def api_get_countries():
    countries = set()
    for provider in get_pools():
        for name in get_pool(provider, "..name"):
            countries.add(name[:2])
    return sorted(countries)


@v3_api.post("/seed")
def api_seed():
    wipe_pool()
    try:
        write_pool("sparx", convert_sparx(get_sparx_data()).model_dump())
    except:
        pass
    try:
        write_pool("dfs", convert_dfs(get_dfs_areas()).model_dump())
    except:
        pass
    try:
        write_pool("eaup", convert_eaup(get_eaup()).model_dump())
    except:
        pass
    return None
