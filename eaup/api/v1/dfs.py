from datetime import datetime, timedelta

from fastapi import APIRouter

from eaup.dfs import parse_html, Dfs_Aup
from eaup.services import get_dfs_aup

dfs_api = APIRouter(prefix="/dfs")


@dfs_api.get("/areas/", response_model=Dfs_Aup)
def get_dfs_data(start_datetime: datetime = None, end_datetime: datetime = None):
    start = start_datetime if start_datetime else datetime.now()
    end = end_datetime if end_datetime else datetime.now() + timedelta(days=1)
    aup_html = get_dfs_aup(start_datetime=start, end_datetime=end)
    return parse_html(aup_html)
