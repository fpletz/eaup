from fastapi import APIRouter

from eaup.country_codes import COUNTRY_CODES
from eaup.eaup import area_list_country_data
from eaup.eaup import Eaup, merge_to_eaup
from eaup.storage import eaup_data

areas_api = APIRouter(prefix="/areas")


@areas_api.get("/", response_model=Eaup)
def get_all_areas():
    return eaup_data()


@areas_api.get("/{country_code}/", response_model=Eaup)
def get_country_areas(country_code: COUNTRY_CODES):
    eaup = eaup_data()
    eaup_country_areas = area_list_country_data(
        eaup_area_list=eaup.areas, country_code=country_code
    )
    return merge_to_eaup(info=eaup.notice_info, area_list=eaup_country_areas)
