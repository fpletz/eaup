from fastapi import APIRouter

from eaup.api.v1.areas import areas_api
from eaup.api.v1.dfs import dfs_api
from eaup.api.v1.topsky import topsky_api

v1_api = APIRouter(prefix="/v1", tags=["v1"])

v1_api.include_router(areas_api)
v1_api.include_router(dfs_api)
v1_api.include_router(topsky_api)
