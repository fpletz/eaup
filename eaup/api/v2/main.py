from fastapi import APIRouter

from eaup.api.v2.areas import areas_api
from eaup.api.v2.dfs import dfs_api
from eaup.api.v2.sparx import sparx_api

v2_api = APIRouter(prefix="/v2", tags=["v2"])

v2_api.include_router(areas_api)
v2_api.include_router(dfs_api)
v2_api.include_router(sparx_api)
