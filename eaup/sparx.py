from datetime import datetime
from typing import Optional

from eaup.services import get_sparx_aup

from pydantic import BaseModel, field_validator, field_serializer


class SparxArea(BaseModel):
    airspace_name: str
    min_alt: int
    max_alt: int
    time_from: datetime
    time_till: datetime
    status: int
    updated_at: datetime

    @field_validator("time_from", "time_till", mode="before")
    @classmethod
    def serialize_times(cls, v: str) -> datetime:
        return datetime.fromisoformat(v)

    @field_validator("airspace_name", mode="before")
    @classmethod
    def serialize_airspace_name(cls, v: str) -> str:
        return f"EI{v}"

    @field_serializer("time_from")
    def serialize_start_datetime(self, dt: datetime, _info):
        return dt.isoformat()

    @field_serializer("time_till")
    def serialize_end_datetime(self, dt: datetime, _info):
        return dt.isoformat()

    @field_serializer("updated_at")
    def serialize_updated_at(self, dt: datetime, _info):
        return dt.isoformat()

    @field_validator("min_alt", "max_alt", mode="before")
    @classmethod
    def serialize_alt(cls, v: str) -> int:
        return int(int(v) / 100)


class Sparx(BaseModel):
    areas: list[Optional[SparxArea]] = []


def get_sparx_data() -> Sparx:
    return Sparx(areas=[SparxArea(**area) for area in get_sparx_aup()])
